package com.m_movie.presentation.navigation

import android.util.Log
import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.layout.padding
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.navigation.NavHostController
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import com.m_movie.presentation.screen.detailmovie.DetailMovieScreen
import com.m_movie.presentation.screen.genre.GenreScreen
import com.m_movie.presentation.screen.movie.MovieScreen
import com.m_movie.presentation.screen.review.ReviewScreen
import com.m_movie.presentation.screen.splash.SplashScreen
import com.m_movie.presentation.screen.video.VideoScreen

/***
 * Created By Mohammad Toriq on 04/02/2024
 */
@Composable
fun MainNavHost(
    navController: NavHostController
) {
    NavHost(
        navController = navController,
        startDestination = Screen.GenresScreen.route
    ) {
        composable(route = Screen.SplashScreen.route) {
            SplashScreen(
                navigateToMain = {
                    navController.navigate(Screen.GenresScreen.route){
                        popUpTo(Screen.SplashScreen.route){
                            inclusive = true
                        }
                    }
                }
            )
        }
        composable(route = Screen.GenresScreen.route) {
            GenreScreen(
                navigateToList = { id,title ->
                    navController.navigate(
                        Screen.MoviesScreen
                            .sendData(id,title)
                    )
                }
            )
        }
        composable(route = Screen.MoviesScreen.route){
            var id = it.arguments?.getString("id") ?: "0"
            var title = it.arguments?.getString("title") ?: ""
            MovieScreen(
                id = id,
                title = title,
                navigateToDetail = {id ->
                    navController.navigate(
                        Screen.DetailMovieScreen
                            .sendData(id)
                    )
                },
                navigateBack = {
                    navController.navigateUp()
                }
            )
        }
        composable(route = Screen.DetailMovieScreen.route){
            var id = it.arguments?.getString("id") ?: "0"
            DetailMovieScreen(
                id = id.toLong(),
                navigateBack = {
                    navController.navigateUp()
                },
                navigateToReviews = {id,title->

                    Log.d("OkCheck","check DetailMovieScreen route navigateToReviews : ${id} & ${title}")
                    navController.navigate(
                        Screen.ReviewsScreen
                            .sendData(id,title)
                    )
                },
                navigateToVideo = {key->
                    navController.navigate(
                        Screen.VideoScreen
                            .sendData(key)
                    )
                }
            )
        }
        composable(route = Screen.ReviewsScreen.route){
            var id = it.arguments?.getString("id") ?: "0"
            var title = it.arguments?.getString("title") ?: ""
            Log.d("OkCheck","check ReviewsScreen route data : ${id} & ${title}")
            ReviewScreen(
                id,
                title,
                navigateBack = {
                    navController.navigateUp()
                })
        }
        composable(route = Screen.VideoScreen.route){
            var key =  it.arguments?.getString("key") ?: ""
            VideoScreen(
                key = key,
                navigateBack = {
                    navController.navigateUp()
                }
            )
        }
    }
}