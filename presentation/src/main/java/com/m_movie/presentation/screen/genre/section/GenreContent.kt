package com.m_movie.presentation.screen.genre.section

import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.lazy.grid.GridCells
import androidx.compose.foundation.lazy.grid.GridItemSpan
import androidx.compose.foundation.lazy.grid.LazyVerticalGrid
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.unit.dp
import com.m_movie.domain.model.Genres
import com.m_movie.presentation.component.EmptyData
import com.m_movie.presentation.component.GenreItem
import com.m_movie.presentation.screen.genre.GenreViewModel
import com.m_movie.presentation.ui.constant.ResString

/***
 * Created By Mohammad Toriq on 04/02/2024
 */
@Composable
fun GenreContent(
    modifier: Modifier,
    data: Genres,
    viewModel: GenreViewModel,
    navigateToList: (String,String) -> Unit,
) {
    LazyVerticalGrid(
        modifier = modifier
            .fillMaxSize(),
        columns = GridCells.Adaptive(120.dp),
        content = {
            items (count = data.genres.size){ index ->
                var genre = data.genres[index]
                GenreItem(
                    modifier = Modifier,
                    data = genre,
                    onClick = {
                        navigateToList(genre.id.toString(),genre.name!!)
                    }
                )
            }
        },
        contentPadding = PaddingValues(8.dp),
    )
}