package com.m_movie.presentation.screen.movie

import androidx.lifecycle.ViewModel
import com.m_movie.domain.model.Movies
import com.m_movie.domain.usecase.GetDiscoverMoviesUseCase
import com.m_movie.presentation.util.UiState
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.flow.catch
import kotlinx.coroutines.launch
import javax.inject.Inject

/***
 * Created By Mohammad Toriq on 04/02/2024
 */
@HiltViewModel
class MovieViewModel  @Inject constructor(
    private val getDiscoverMoviesUseCase: GetDiscoverMoviesUseCase
): ViewModel() {

    private val _uiState: MutableStateFlow<UiState<Movies>> = MutableStateFlow(UiState.Loading())
    val uiState: StateFlow<UiState<Movies>> = _uiState

    private val _isLoading = MutableStateFlow(false)
    val isLoading = _isLoading.asStateFlow()

    private val _page = MutableStateFlow(1)
    val page = _page.asStateFlow()

    private val _isMax = MutableStateFlow(false)
    val isMax = _isMax.asStateFlow()

    var withGenres = ""

    fun refresh(){
        _page.value = 1
        _uiState.value = UiState.Loading()
    }

    fun addCounterPage(){
        _page.value ++
        getMovie(withGenres)
    }

    fun getMovie(withGenres :String) {
        this.withGenres = withGenres
        _isLoading.value = true
        CoroutineScope(Dispatchers.IO).launch {
            try {
                var params = listOf(page.value.toString() , withGenres)
                getDiscoverMoviesUseCase.execute(params)
                    .catch {
                        _isMax.value = true
                        _isLoading.value = false
                        _uiState.value = UiState.Error(it.message.toString())
                    }
                    .collect{ response ->
                        var list = _uiState.value.data?.movies
                        if(list == null){
                            list = arrayListOf()
                        }
                        if(page.value == 1){
                            list = arrayListOf()
                        }
                        response.movies.forEach {
                            list.add(it)
                        }
                        response.movies = list

                        if(page.value.toLong() == response.totalPages){
                            _isMax.value = true
                        }else{
                            _isMax.value = false
                        }
                        _isLoading.value = false
                        _uiState.value = UiState.Success(response)
                    }
            } catch (e: Exception) {
                _isMax.value = true
                _isLoading.value = false
                _uiState.value = UiState.Error(e.message.toString())
            }
        }
    }
}
