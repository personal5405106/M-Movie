package com.m_movie.presentation.screen.review

import android.util.Log
import androidx.lifecycle.ViewModel
import com.m_movie.domain.model.Reviews
import com.m_movie.domain.usecase.GetMovieReviewsUseCase
import com.m_movie.presentation.util.UiState
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.flow.catch
import kotlinx.coroutines.launch
import javax.inject.Inject

/***
 * Created By Mohammad Toriq on 05/02/2024
 */
@HiltViewModel
class ReviewViewModel @Inject constructor(
    private val getMovieReviewsUseCase: GetMovieReviewsUseCase
): ViewModel(){
    private val _uiState : MutableStateFlow<UiState<Reviews>> = MutableStateFlow(UiState.Loading())
    val uiState : StateFlow<UiState<Reviews>> = _uiState

    private val _isLoading = MutableStateFlow(false)
    val isLoading = _isLoading.asStateFlow()

    private val _page = MutableStateFlow(1)
    val page = _page.asStateFlow()

    private val _isMax = MutableStateFlow(false)
    val isMax = _isMax.asStateFlow()
    var id  = "-1"

    fun refresh(){
        _page.value = 1
        _uiState.value = UiState.Loading()
    }

    fun addCounterPage(){
        _page.value ++
        getReviews(id)
    }

    fun getReviews(id:String){
        Log.d("OkCheck","check getReviews : ${id}")
        this.id  = id
        _isLoading.value = true
        CoroutineScope(Dispatchers.IO).launch {
            try {
                var params = listOf(id, page.value.toString())
                getMovieReviewsUseCase.execute(params)
                    .catch {
                        Log.d("OkCheck","check getReviews catch : ${id}")
                        _isLoading.value = false
                        _isMax.value = true
                        _uiState.value = UiState.Error(it.message.toString())

                    }.collect{response ->
                        Log.d("OkCheck","check getReviews collect : ${id}")
                        var list = _uiState.value.data?.reviews
                        if(list == null){
                            list = arrayListOf()
                        }
                        if(page.value == 1){
                            list = arrayListOf()
                        }
                        response.reviews.forEach {
                            list.add(it)
                        }
                        response.reviews = list

                        if(page.value.toLong() == response.totalPages){
                            _isMax.value = true
                        }else{
                            _isMax.value = false
                        }
                        _isLoading.value = false
                        _uiState.value = UiState.Success(response)
                    }
            }catch (e:Exception){
                Log.d("OkCheck","check getReviews Exception : ${id}")
                _isLoading.value = false
                _isMax.value = true
                _uiState.value = UiState.Error(e.message.toString())

            }
        }

    }
}