package com.m_movie.presentation.screen.splash

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.launch
import javax.inject.Inject

/***
 * Created By Mohammad Toriq on 04/02/2024
 */
@HiltViewModel
class SplashViewModel @Inject constructor(
): ViewModel() {

    private val _isLoading = MutableStateFlow(true)
    val isLoading = _isLoading.asStateFlow()

    init {
        loading()
    }
    fun setLoading(action:()->Unit){
        _isLoading.value = true
        viewModelScope.launch {
            delay(500)
            action()
        }
    }

    fun loading() {
        viewModelScope.launch {
            delay(1000)
            _isLoading.value = false
        }
    }
}