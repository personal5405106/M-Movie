package com.m_movie.presentation.screen.detailmovie

import androidx.activity.compose.BackHandler
import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.padding
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.ArrowBack
import androidx.compose.material3.CenterAlignedTopAppBar
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.IconButton
import androidx.compose.material3.Scaffold
import androidx.compose.material3.Surface
import androidx.compose.material3.Text
import androidx.compose.material3.TopAppBarDefaults
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.ColorFilter
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.hilt.navigation.compose.hiltViewModel
import androidx.lifecycle.compose.collectAsStateWithLifecycle
import com.m_movie.presentation.util.UiState
import com.m_movie.presentation.component.EmptyData
import com.m_movie.presentation.component.ProgressLoading
import com.m_movie.presentation.screen.detailmovie.section.DetailMovieContent
import com.m_movie.presentation.ui.constant.ResString
import com.m_movie.presentation.ui.constant.primary

/***
 * Created By Mohammad Toriq on 05/02/2024
 */
@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun DetailMovieScreen(
    id:Long,
    viewModel : DetailMovieViewModel = hiltViewModel(),
    navigateBack : () -> Unit,
    navigateToReviews : (String,String) -> Unit,
    navigateToVideo : (String) -> Unit
) {
    val load by viewModel.isLoading.collectAsStateWithLifecycle()

    BackHandler {
        navigateBack()
    }
    Scaffold(
        topBar ={
            Surface (shadowElevation = 1.dp){
                CenterAlignedTopAppBar(
                    title = {
                        Text(
                            text = "Detail Movie",
                            fontSize = 16.sp,
                            fontWeight = FontWeight.Bold
                        )
                    },
                    colors = TopAppBarDefaults.mediumTopAppBarColors(
                        containerColor = primary,
                        titleContentColor = Color.White,
                    ),
                    navigationIcon = {
                        IconButton(
                            onClick = navigateBack
                        ) {
                            Image(
                                imageVector = Icons.Filled.ArrowBack,
                                colorFilter = ColorFilter.tint(Color.White),
                                contentDescription = "Back"
                            )
                        }
                    },
                )
            }
        },
        content = {
            Box(
                modifier = Modifier
                    .fillMaxSize()
                    .background(Color.White)
                    .padding(it)
            ) {

                viewModel.uiState.collectAsState(initial = UiState.Loading()).value.let { uiState ->
                    when (uiState) {
                        is UiState.Loading -> {
                            ProgressLoading()
                            if(!load){
                                viewModel.getDetailMovie(id)
                            }
                        }

                        is UiState.Success -> {
                            if(!load){
                                if(uiState.data == null){
                                    EmptyData(message = ResString.DATA_NOT_FOUND)
                                }else{
                                    DetailMovieContent(
                                        modifier = Modifier,
                                        data = uiState.data,
                                        viewModel = viewModel,
                                        navigateToReviews = navigateToReviews,
                                        navigateToVideo = navigateToVideo,
                                    )
                                }
                            }else{
                                ProgressLoading()
                            }
                        }

                        is UiState.Error -> {
                            EmptyData(message = ResString.DATA_NOT_FOUND)
                        }
                    }
                }
            }
        }
    )
}