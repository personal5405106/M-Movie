@file:OptIn(ExperimentalMaterial3Api::class)
package com.m_movie.presentation.screen.video

import android.annotation.SuppressLint
import android.content.pm.ActivityInfo
import androidx.activity.ComponentActivity
import androidx.activity.compose.BackHandler
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.padding
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.Scaffold
import androidx.compose.runtime.Composable
import androidx.compose.runtime.DisposableEffect
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.unit.dp
import androidx.compose.ui.viewinterop.AndroidView
import com.google.accompanist.systemuicontroller.SystemUiController
import com.google.accompanist.systemuicontroller.rememberSystemUiController
import com.pierfrancescosoffritti.androidyoutubeplayer.core.player.YouTubePlayer
import com.pierfrancescosoffritti.androidyoutubeplayer.core.player.listeners.AbstractYouTubePlayerListener
import com.pierfrancescosoffritti.androidyoutubeplayer.core.player.views.YouTubePlayerView

/***
 * Created By Mohammad Toriq on 05/02/2024
 */
@Composable
@SuppressLint("UnusedMaterial3ScaffoldPaddingParameter")
fun VideoScreen (
    key:String,
    navigateBack : ()-> Unit
){
    BackHandler {
        navigateBack()
    }
    val activity = LocalContext.current as ComponentActivity
    val systemUiController: SystemUiController = rememberSystemUiController()

    systemUiController.isStatusBarVisible = false // Status bar
    systemUiController.isNavigationBarVisible = false // Navigation bar
    systemUiController.isSystemBarsVisible = false // Status & Navigation bars

    LockScreenOrientation(activity = activity, orientation = ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE)

    Scaffold(
        modifier = Modifier
            .fillMaxSize(),
        content = {
            Column(
                modifier = Modifier
                    .padding(it)
                    .background(color = Color.Black)
                    .fillMaxSize()) {
                AndroidView(
                    modifier = Modifier.padding(50.dp),
                    factory = {
                        var view = YouTubePlayerView(it)
                        view.addYouTubePlayerListener(
                            object : AbstractYouTubePlayerListener() {
                                override fun onReady(youTubePlayer: YouTubePlayer) {
                                    super.onReady(youTubePlayer)
                                    youTubePlayer.loadVideo(key, 0f)
                                }
                            }
                        )
                        view
                    })
            }
        }
    )
}

@Composable
fun LockScreenOrientation(activity: ComponentActivity,
                          orientation: Int) {
    DisposableEffect(orientation) {
        val originalOrientation = activity.requestedOrientation
        activity.requestedOrientation = orientation
        onDispose {
            activity.requestedOrientation = originalOrientation
        }
    }
}