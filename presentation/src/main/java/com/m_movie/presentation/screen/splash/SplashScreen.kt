package com.m_movie.presentation.screen.splash

import androidx.compose.animation.core.Animatable
import androidx.compose.animation.core.tween
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.padding
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.Scaffold
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.unit.sp
import androidx.hilt.navigation.compose.hiltViewModel
import androidx.compose.runtime.*
import androidx.lifecycle.compose.collectAsStateWithLifecycle
import com.m_movie.presentation.ui.constant.ResString
import com.m_movie.presentation.ui.constant.primary

/***
 * Created By Mohammad Toriq on 04/02/2024
 */
@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun SplashScreen(
    viewModel: SplashViewModel = hiltViewModel(),
    navigateToMain: () -> Unit,
) {
    val load by viewModel.isLoading.collectAsStateWithLifecycle()
    if(!load){
        viewModel.setLoading(navigateToMain)
    }

//    val degrees = remember { Animatable(0f) }
//    LaunchedEffect(key1 = true) {
//        degrees.animateTo(
//            targetValue = 360f,
//            animationSpec = tween(
//                durationMillis = 1000,
//                delayMillis = 200
//            )
//        )
//    }

    Scaffold(
        content = {
            Box(
                contentAlignment = Alignment.Center,
                modifier = Modifier
                    .fillMaxSize()
                    .background(primary)
                    .padding(it)
            ) {
                Column (
                    modifier = Modifier,
                    verticalArrangement = Arrangement.Center,
                    horizontalAlignment  = Alignment.CenterHorizontally,
                )
                {

                    Text(
                        text = ResString.APP_NAME,
                        fontSize = 38.sp,
                        fontWeight = FontWeight.Bold,
                        color = Color.White,
                        modifier = Modifier,
                    )
                }
            }
        }
    )
}