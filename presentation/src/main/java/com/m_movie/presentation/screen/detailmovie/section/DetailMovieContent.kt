package com.m_movie.presentation.screen.detailmovie.section

import android.util.Log
import android.view.View
import androidx.activity.ComponentActivity
import androidx.activity.OnBackPressedCallback
import androidx.compose.foundation.BorderStroke
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material3.Button
import androidx.compose.material3.ButtonDefaults
import androidx.compose.material3.OutlinedButton
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.MutableState
import androidx.compose.runtime.collectAsState
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.ui.draw.clip
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.viewinterop.AndroidView
import androidx.compose.ui.window.Dialog
import coil.compose.AsyncImage
import com.m_movie.data.source.remote.ApiService
import com.m_movie.domain.model.Movie
import com.m_movie.presentation.util.UiState
import com.m_movie.presentation.util.Util
import com.m_movie.presentation.screen.detailmovie.DetailMovieViewModel
import com.m_movie.presentation.ui.constant.FormatTime
import com.m_movie.presentation.ui.constant.primary
import com.m_movie.presentation.ui.constant.primary2
import com.pierfrancescosoffritti.androidyoutubeplayer.core.player.YouTubePlayer
import com.pierfrancescosoffritti.androidyoutubeplayer.core.player.listeners.AbstractYouTubePlayerListener
import com.pierfrancescosoffritti.androidyoutubeplayer.core.player.listeners.FullscreenListener
import com.pierfrancescosoffritti.androidyoutubeplayer.core.player.options.IFramePlayerOptions
import com.pierfrancescosoffritti.androidyoutubeplayer.core.player.views.YouTubePlayerView

/***
 * Created By Mohammad Toriq on 05/02/2024
 */
@Composable
fun DetailMovieContent(
    modifier: Modifier,
    data: Movie,
    viewModel: DetailMovieViewModel,
    navigateToReviews: (String,String) -> Unit,
    navigateToVideo: (String) -> Unit,
) {
    val showDialog =  remember { mutableStateOf(false) }
    val videoKey =  remember { mutableStateOf("") }
    if(showDialog.value){
        showDetailMovieVideo(videoKey = videoKey, navigateToVideo = navigateToVideo,showDialog)
    }
    Column(
        modifier = Modifier
            .fillMaxSize(),
        verticalArrangement = Arrangement.Top
    ) {
        Row (
            modifier = Modifier
                .background(color = primary2),
            verticalAlignment = Alignment.CenterVertically
        ){
            AsyncImage(
                model = ApiService.IMAGE_BASE_URL + data.imageUrl,
                contentDescription = null,
                alignment = Alignment.Center,
                modifier = Modifier
                    .weight(1f)
                    .padding(
                        start = 10.dp,
                        end = 5.dp,
                        top = 10.dp,
                        bottom = 10.dp
                    )
                    .clip(
                        shape = RoundedCornerShape(4.dp)
                    )
            )
            Column (
                modifier = Modifier
                    .weight(2f)
                    .padding(
                        start = 5.dp,
                        end = 10.dp,
                        top = 10.dp,
                        bottom = 10.dp
                    )
            ){
                Text(
                    modifier = Modifier
                        .fillMaxWidth()
                        .padding(horizontal = 5.dp),
                    text = data.title!!,
                    fontSize = 16.sp,
                    color = Color.White,
                    fontWeight = FontWeight.Bold
                )
                Spacer(modifier = Modifier.height(10.dp))
                var genre = ""
                data.genres.forEach {
                    var isComma = true
                    if(genre.equals("")){
                        isComma = false
                        genre = "Genre : "
                    }
                    if(isComma){
                        genre += ", "
                    }
                    genre += it.name

                }
                if(!genre.equals("")){
                    Text(
                        modifier = Modifier
                            .fillMaxWidth()
                            .padding(5.dp),
                        text = genre,
                        fontSize = 13.sp,
                        color = Color.White,
                    )
                }
                if(data.releaseDate != null){
                    Text(
                        modifier = Modifier
                            .fillMaxWidth()
                            .padding(5.dp),
                        text = "Release : "+ Util.convertDate(data.releaseDate!!,
                            FormatTime.DATE_OUT_FORMAT_DEF2,
                            FormatTime.DATE_OUT_FORMAT_DEF4),
                        fontSize = 13.sp,
                        color = Color.White,
                    )
                }
                if(data.runtime > 0){
                    Text(
                        modifier = Modifier
                            .fillMaxWidth()
                            .padding(5.dp),
                        text = "Durasi : "+ Util.getShowTime(data.runtime),
                        fontSize = 13.sp,
                        color = Color.White,
                    )
                }
                if(data.voteAverage > 0){
                    Text(
                        modifier = Modifier
                            .fillMaxWidth()
                            .padding(5.dp),
                        text = "Rating : "+String.format("%.1f", data.voteAverage).toDouble()
                        ,
                        fontSize = 13.sp,
                        color = Color.White,
                    )
                }
            }
        }

        Text(
            modifier = Modifier
                .fillMaxWidth()
                .padding(
                    top = 15.dp,
                    start = 15.dp,
                    end = 15.dp
                ),
            text = "Description",
            fontSize = 18.sp,
            color = primary,
            fontWeight = FontWeight.Bold
        )
        Text(
            modifier = Modifier
                .fillMaxWidth()
                .padding(horizontal = 15.dp, vertical = 10.dp),
            text = data.desc!!,
            fontSize = 14.sp,
            color = Color.Gray,
        )
        Row{
            viewModel.videoState.collectAsState(initial = UiState.Loading()).value.let { uiState ->
                when (uiState) {
                    is UiState.Loading -> {
                    }

                    is UiState.Success -> {
                        if(uiState.data != null){
                            videoKey.value = uiState.data.key!!
                            Button(
                                modifier = Modifier
                                    .fillMaxWidth()
                                    .weight(1f)
                                    .padding(5.dp),
                                colors = ButtonDefaults.buttonColors(
                                    containerColor = primary2
                                ),
                                shape = RoundedCornerShape(10),
                                onClick = {
                                    showDialog.value = true
                                },
                                contentPadding = PaddingValues()
                            ) {

                                Text(
                                    modifier = Modifier
                                        .fillMaxWidth()
                                        .padding(10.dp),
                                    text = "Show "+uiState.data.type!!,
                                    fontSize = 14.sp,
                                    color = Color.White,
                                    textAlign = TextAlign.Center,
                                    fontWeight = FontWeight.Bold
                                )
                            }
                        }
                    }

                    is UiState.Error -> {
                    }
                }
            }
            OutlinedButton(
                modifier = Modifier
                    .fillMaxWidth()
                    .weight(1f)
                    .padding(5.dp),
                shape = RoundedCornerShape(10),
                border = BorderStroke(1.dp, primary2),
                onClick = {
//                    navController.navigate(
//                        Screen.ReviewsScreen
//                            .sendData(movie.id.toString(), movie.title!!)
//                    )
                    Log.d("OkCheck","check DetailMovieContent data : ${data.id} & ${data.title}")
                    navigateToReviews(data.id.toString(),data.title!!)
                },
                contentPadding = PaddingValues()
            ) {
                Text(
                    modifier = Modifier
                        .fillMaxWidth()
                        .padding(10.dp),
                    text = "Reviews",
                    fontSize = 14.sp,
                    color = primary2,
                    textAlign = TextAlign.Center,
                    fontWeight = FontWeight.Bold
                )
            }
        }

    }
}

private var isFullscreen = false

@Composable
fun showDetailMovieVideo(
    videoKey: MutableState<String>,
    navigateToVideo: (String) -> Unit,
    openDialogCustom: MutableState<Boolean>
) {
    val activity = LocalContext.current as ComponentActivity
    Dialog(onDismissRequest = { openDialogCustom.value = false }) {
        AndroidView(factory = {
            var view = YouTubePlayerView(it)

            var youTubePlayerHome: YouTubePlayer?= null

            val iFramePlayerOptions = IFramePlayerOptions.Builder()
                .controls(1)
                .fullscreen(1)
                .build()

            view.enableAutomaticInitialization = false

            isFullscreen = false

            view.addFullscreenListener(object : FullscreenListener {
                override fun onEnterFullscreen(fullscreenView: View, exitFullscreen: () -> Unit) {
                    isFullscreen = true
                    openDialogCustom.value = false
                    navigateToVideo(videoKey.value)
                }

                override fun onExitFullscreen() {
                    isFullscreen = false
                }
            })
            view.initialize(object : AbstractYouTubePlayerListener() {
                override fun onReady(youTubePlayer: YouTubePlayer) {
                    youTubePlayerHome = youTubePlayer
                    super.onReady(youTubePlayer)
                    youTubePlayer.loadVideo(videoKey.value, 0f)
                }
            }, iFramePlayerOptions)


            activity.onBackPressedDispatcher.addCallback(
                object : OnBackPressedCallback(true) {
                    override fun handleOnBackPressed() {
                        if (isFullscreen) {
                            if(youTubePlayerHome!=null){
                                youTubePlayerHome!!.toggleFullscreen()
                            }
                        }
                    }
                }
            )
            activity.lifecycle.addObserver(view)
            view
        })
    }

}