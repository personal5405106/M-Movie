package com.m_movie.presentation.screen.detailmovie

import androidx.lifecycle.ViewModel
import com.m_movie.domain.model.Movie
import com.m_movie.domain.model.Video
import com.m_movie.domain.usecase.GetDetailMovieUseCase
import com.m_movie.domain.usecase.GetMovieVideosUseCase
import com.m_movie.presentation.util.UiState
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.flow.catch
import kotlinx.coroutines.launch
import javax.inject.Inject

/***
 * Created By Mohammad Toriq on 05/02/2024
 */
@HiltViewModel
class DetailMovieViewModel @Inject constructor(
    private val getDetailMovieUseCase: GetDetailMovieUseCase,
    private val getMovieVideosUseCase: GetMovieVideosUseCase
): ViewModel() {
    private val _uiState : MutableStateFlow<UiState<Movie>> = MutableStateFlow(UiState.Loading())
    val uiState : StateFlow<UiState<Movie>> = _uiState

    private val _videoState : MutableStateFlow<UiState<Video?>> = MutableStateFlow(UiState.Loading())
    val videoState : StateFlow<UiState<Video?>> = _videoState

    private val _isLoading = MutableStateFlow(false)
    val isLoading = _isLoading.asStateFlow()

    fun getDetailMovie(id:Long){
        _isLoading.value = true
        CoroutineScope(Dispatchers.IO).launch {
            try {
                getDetailMovieUseCase.execute(id)
                    .catch {
                        _uiState.value = UiState.Error(it.message.toString())
                    }.collect{response ->
                        _uiState.value = UiState.Success(response)
                        getMovieVideo(id)
                    }
            }catch (e:Exception){
                _uiState.value = UiState.Error(e.message.toString())
            }
        }
    }
    fun getVideo(videos : List<Video>) : Video?{
        var types = listOf("Trailer","Teaser","Clip")
        var result:Video ?= null
        types.forEach {type ->
            if(result == null){
                videos.forEach { vid ->
                    if(vid.type!!.contains(type,true)){
                        result = vid
                    }
                }
            }
        }
        if(result == null){
            videos.forEach { vid ->
                if(result == null){
                    result = vid
                }
            }
        }
        return result
    }
    fun getMovieVideo(id:Long){
        CoroutineScope(Dispatchers.IO).launch {
            try {
                getMovieVideosUseCase.execute(id)
                    .catch {
                        _isLoading.value = false
                        _videoState.value = UiState.Error(it.message.toString())
                    }.collect{response ->
                        var video = getVideo(response.videos)
                        _isLoading.value = false
                        _videoState.value = UiState.Success(video)
                    }
            }catch (e:Exception){
                _isLoading.value = false
                _videoState.value = UiState.Error(e.message.toString())
            }
        }
    }
}