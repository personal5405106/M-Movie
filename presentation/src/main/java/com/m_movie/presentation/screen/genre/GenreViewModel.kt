package com.m_movie.presentation.screen.genre

import androidx.lifecycle.ViewModel
import com.m_movie.domain.model.Genres
import com.m_movie.domain.usecase.GetGenresUseCase
import com.m_movie.presentation.util.UiState
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.flow.catch
import kotlinx.coroutines.launch
import javax.inject.Inject

/***
 * Created By Mohammad Toriq on 04/02/2024
 */
@HiltViewModel
class GenreViewModel  @Inject constructor(
    private val getGenresUseCase : GetGenresUseCase
): ViewModel() {

    private val _uiState: MutableStateFlow<UiState<Genres>> = MutableStateFlow(UiState.Loading())
    val uiState: StateFlow<UiState<Genres>> = _uiState

    private val _isLoading = MutableStateFlow(false)
    val isLoading = _isLoading.asStateFlow()

    fun getGenres() {
        _isLoading.value = true
        CoroutineScope(Dispatchers.IO).launch {
            try {
                getGenresUseCase.execute(Unit)
                    .catch {
                        _isLoading.value = false
                        _uiState.value = UiState.Error(it.message.toString())
                    }
                    .collect{ response ->
                        _isLoading.value = false
                        _uiState.value = UiState.Success(response)
                    }
            } catch (e: Exception) {
                _isLoading.value = false
                _uiState.value = UiState.Error(e.message.toString())
            }
        }
    }
}
