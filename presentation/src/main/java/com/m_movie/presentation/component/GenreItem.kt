package com.m_movie.presentation.component

import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.m_movie.domain.model.Genre
import com.m_movie.presentation.ui.constant.primary
import com.m_movie.presentation.util.noRippleClickable

/***
 * Created By Mohammad Toriq on 04/02/2024
 */
@Composable
fun GenreItem (
     modifier: Modifier,
     data: Genre,
     onClick: () -> Unit = {}
){
    Box(
        modifier = modifier
        .padding(5.dp)
        .background(
            color = primary,
            shape = RoundedCornerShape(50),
        )
    .noRippleClickable {
        onClick()
    },
    ){
        Text(
            modifier = Modifier
                .fillMaxWidth()
                .padding(10.dp),
            text = data.name!!,
            textAlign = TextAlign.Center,
            fontSize = 15.sp,
            fontWeight = FontWeight.SemiBold
        )
    }
}