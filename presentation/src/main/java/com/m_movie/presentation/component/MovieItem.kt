package com.m_movie.presentation.component

import androidx.compose.foundation.BorderStroke
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material3.OutlinedButton
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import coil.compose.AsyncImage
import com.m_movie.data.source.remote.ApiService
import com.m_movie.domain.model.Movie
import com.m_movie.presentation.ui.constant.primary
import com.m_movie.presentation.util.noRippleClickable

/***
 * Created By Mohammad Toriq on 04/02/2024
 */
@Composable
fun MovieItem (
     modifier: Modifier,
     data: Movie,
     onClick: () -> Unit = {}
){
    OutlinedButton(
        modifier = Modifier
            .padding(all = 3.dp)
        ,
        contentPadding = PaddingValues(),
        border = BorderStroke(0.dp, Color.White),
        shape = RoundedCornerShape(4.dp),
        onClick = fun(){
        }
    ){
        Column(
            modifier = Modifier
                .fillMaxWidth()
                .noRippleClickable{
                    onClick()
                }
        ){
            AsyncImage(
                model = ApiService.IMAGE_BASE_URL+data.imageUrl,
                contentDescription = null,
                contentScale = ContentScale.Crop,
                alignment = Alignment.Center,
                modifier = Modifier
                    .fillMaxWidth()
                    .height(250.dp)
            )
            Text(
                modifier = Modifier
                    .fillMaxWidth()
                    .padding(horizontal = 5.dp, vertical = 10.dp),
                text = data.title!!,
                fontSize = 14.sp,
                color = primary,
                textAlign = TextAlign.Center
            )
//            Text(
//                modifier = Modifier
//                    .fillMaxWidth()
//                    .padding(5.dp),
//                text = Util.convertDate(data.releaseDate,
//                    FormatTime.DATE_OUT_FORMAT_DEF2,
//                    FormatTime.DATE_OUT_FORMAT_DEF3),
//                fontSize = 12.sp,
//                color = Color.Gray,
//            )
        }
    }
}