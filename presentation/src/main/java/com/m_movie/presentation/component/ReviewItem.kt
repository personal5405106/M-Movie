package com.m_movie.presentation.component

import androidx.compose.foundation.BorderStroke
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material3.OutlinedButton
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.setValue
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.draw.drawBehind
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.res.colorResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.text.style.TextOverflow
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import coil.compose.AsyncImage
import com.m_movie.data.source.remote.ApiService
import com.m_movie.domain.model.Review
import com.m_movie.presentation.screen.review.ReviewViewModel
import com.m_movie.presentation.ui.constant.FormatTime
import com.m_movie.presentation.ui.constant.blue
import com.m_movie.presentation.ui.constant.grey
import com.m_movie.presentation.ui.constant.grey2
import com.m_movie.presentation.ui.constant.primary
import com.m_movie.presentation.ui.constant.primary2
import com.m_movie.presentation.util.NoRippleInteractionSource
import com.m_movie.presentation.util.Util

/***
 * Created By Mohammad Toriq on 05/02/2024
 */
@Composable
fun ReviewItem(
    modifier: Modifier,
    data:Review,
    viewModel: ReviewViewModel
) {
    var isExpanded by remember { mutableStateOf(false) }
    OutlinedButton(
        modifier = Modifier
            .padding(all = 5.dp),
        interactionSource = remember { NoRippleInteractionSource() },
        contentPadding = PaddingValues(),
        border = BorderStroke(1.dp, primary),
        shape = RoundedCornerShape(4.dp),
        onClick = fun(){
            isExpanded = !isExpanded
        }
    ){
        Column(
            modifier = Modifier
                .padding(all = 5.dp)
                .fillMaxWidth()
        ){
            Row(
                verticalAlignment = Alignment.CenterVertically
            ){
                if(data.authorDetails.avatarPath!=null){
                    AsyncImage(
                        model = ApiService.IMAGE_BASE_URL+data.authorDetails.avatarPath!!,
                        contentDescription = null,
                        contentScale = ContentScale.Crop,
                        alignment = Alignment.Center,
                        modifier = Modifier
                            .padding(5.dp)
                            .clip(CircleShape)
                            .size(35.dp)
                    )
                }else{
                    Text(
                        modifier = Modifier
                            .padding(15.dp)
                            .drawBehind {
                                drawCircle(
                                    color = grey2,
                                    radius = this.size.maxDimension
                                )
                            },
                        text = data.author!!.substring(0,1),
                    )
                }
                Column {
                    Text(
                        modifier = Modifier
                            .fillMaxWidth()
                            .padding(
                                top = 5.dp,
                                start = 5.dp,
                                end = 5.dp
                            ),
                        text = data.author!!,
                        fontSize = 14.sp,
                        fontWeight = FontWeight.Bold,
                        color = primary,
                    )
                    Text(
                        modifier = Modifier
                            .fillMaxWidth()
                            .padding(
                                top = 5.dp,
                                start = 5.dp,
                                end = 5.dp
                            ),
                        text = "on "+Util.convertDate(
                            data.createdAt!!,
                            FormatTime.DATE_OUT_FORMAT_DEF1,
                            FormatTime.DATE_OUT_FORMAT_DEF4),
                        fontSize = 12.sp,
                        color = primary2,
                    )
                }
            }
            Text(
                modifier = Modifier
                    .fillMaxWidth()
                    .padding(5.dp),
                text = data.content!!,
                fontSize = 12.sp,
                color = primary,
                maxLines = if(isExpanded) Int.MAX_VALUE else 4,
                overflow = TextOverflow.Ellipsis,
            )
            Text(
                modifier = Modifier
                    .fillMaxWidth()
                    .padding(vertical = 10.dp, horizontal = 5.dp),
                text = if(isExpanded) "Sembunyikan" else "Selengkapnya",
                fontSize = 12.sp,
                textAlign = TextAlign.End,
                color = blue,
            )
        }
    }
}