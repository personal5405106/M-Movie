package com.m_movie.presentation.ui.constant

import androidx.compose.ui.graphics.Color

/***
 * Created By Mohammad Toriq on 04/02/2024
 */
val primary = Color(0xFF032541)
val primary2 = Color(0xE6032541)
val secondary = Color(0xFF032541)
val tertiary = Color(0xFF032541)
val grey = Color(0x888888)
val grey2 = Color(0x4B888888)
val blue = Color(0xFF1676C5)