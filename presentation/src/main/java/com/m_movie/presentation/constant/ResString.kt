package com.m_movie.presentation.ui.constant

/***
 * Created By Mohammad Toriq on 04/02/2024
 */
class ResString {
    companion object {
        const val APP_NAME = "M-Movie"
        const val LOADING = "Loading …"
        const val DATA_NOT_FOUND = "Data not Found"
        const val NO_REVIEW = "No Review"
        const val NO_MOVIE = "No Movie"
        const val NO_GENRE = "No Genre"
    }
}

class FormatTime{
    companion object{
        val DATE_OUT_FORMAT_DEF0 = "yyyy-MM-dd'T'HH:mm:ss'Z'"
        val DATE_OUT_FORMAT_DEF1 = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"
        val DATE_OUT_FORMAT_DEF2 = "yyyy-MM-dd"
        val DATE_OUT_FORMAT_DEF3 = "MMM dd, yyyy"
        val DATE_OUT_FORMAT_DEF4 = "dd MMMM yyyy"
    }
}