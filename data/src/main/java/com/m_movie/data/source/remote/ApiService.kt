package com.m_movie.data.source.remote

import com.m_movie.data.source.remote.dto.GenresDto
import com.m_movie.data.source.remote.dto.MovieDto
import com.m_movie.data.source.remote.dto.MoviesDto
import com.m_movie.data.source.remote.dto.ReviewsDto
import com.m_movie.data.source.remote.dto.VideosDto
import retrofit2.http.GET
import retrofit2.http.Query
import retrofit2.http.Path

/***
 * Created By Mohammad Toriq on 04/02/2024
 */
interface ApiService {
    companion object {
        const val BASE_URL = "https://api.themoviedb.org/3/"
        const val IMAGE_BASE_URL = "https://image.tmdb.org/t/p/w185";
        const val TOKEN: String = "eyJhbGciOiJIUzI1NiJ9.eyJhdWQiOiI3ZDhiZTZlOTE5YTdiMjMwMmM5MjlhYzg3NDIwODM0MSIsInN1YiI6IjY1MmY5MDI2YTgwMjM2MDBmZDJkODdhNCIsInNjb3BlcyI6WyJhcGlfcmVhZCJdLCJ2ZXJzaW9uIjoxfQ.ap13eLyCoYdiBdoxl_NC4zIMqqwDoKoBfuoYBHNWdU8"
    }

    @GET("discover/movie")
    suspend fun getDiscoverMovies(
        @Query(value = "page", encoded = true) page: Int,
        @Query(value = "with_genres", encoded = true) withGenres: String?
    ): MoviesDto

    @GET("genre/movie/list")
    suspend fun getGenres(
    ): GenresDto

    @GET("movie/{id}")
    suspend fun getDetailMovie(
        @Path(value = "id", encoded = true) id: Long,
    ): MovieDto

    @GET("movie/{id}/reviews")
    suspend fun getMovieReviews(
        @Path(value = "id", encoded = true) id: Long,
        @Query(value = "page", encoded = true) page: Int,
    ): ReviewsDto

    @GET("movie/{id}/videos")
    suspend fun getMovieVideos(
        @Path(value = "id", encoded = true) id: Long,
    ): VideosDto
}