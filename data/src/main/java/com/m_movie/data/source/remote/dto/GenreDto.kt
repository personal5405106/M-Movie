package com.m_movie.data.source.remote.dto

import com.google.gson.annotations.SerializedName
import com.m_movie.domain.model.Genre

/***
 * Created By Mohammad Toriq on 04/02/2024
 */
data class GenreDto (
    @SerializedName("id")
    var id: Long = 0,
    @SerializedName("name")
    var name: String? = null,
)

fun GenreDto.toGenre(): Genre {
    return Genre(
        id = id,
        name = name,
    )
}