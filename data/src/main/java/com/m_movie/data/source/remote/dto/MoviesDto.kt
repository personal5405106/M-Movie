package com.m_movie.data.source.remote.dto

import com.google.gson.annotations.SerializedName
import com.m_movie.domain.model.Movie
import com.m_movie.domain.model.Movies

/***
 * Created By Mohammad Toriq on 04/02/2024
 */
data class MoviesDto (
    @SerializedName("page")
    var page: Long = 0,
    @SerializedName("results")
    var movies: List<MovieDto> = listOf(),
    @SerializedName("total_pages")
    var totalPages: Long = 0,
    @SerializedName("total_results")
    var totalResults: Long = 0,
)

fun MoviesDto.toMovies(): Movies {
    var arrayList = arrayListOf<Movie>()
    var list = movies.map { it.toMovie() }
    list.forEach {
        arrayList.add(it)
    }
    return Movies(
        page = page,
        movies = arrayList,
        totalPages = totalPages,
        totalResults = totalResults,
    )
}