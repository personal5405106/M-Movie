package com.m_movie.data.source.remote.dto

import com.google.gson.annotations.SerializedName
import com.m_movie.domain.model.Genres

/***
 * Created By Mohammad Toriq on 04/02/2024
 */
data class GenresDto (
    @SerializedName("genres")
    var genres : List<GenreDto> = listOf(),
)

fun GenresDto.toGenres(): Genres {
    return Genres(
        genres = genres.map { it.toGenre() }.toList(),
    )
}