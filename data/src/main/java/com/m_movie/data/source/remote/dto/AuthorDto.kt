package com.m_movie.data.source.remote.dto

import com.google.gson.annotations.SerializedName
import com.m_movie.domain.model.Author

/***
 * Created By Mohammad Toriq on 04/02/2024
 */
data class AuthorDto(
    @SerializedName("name")
    var name: String? = null,
    @SerializedName("username")
    var username: String? = null,
    @SerializedName("avatar_path")
    var avatarPath: String? = null,
    @SerializedName("rating")
    var rating: Double? = null,
)

fun AuthorDto.toAuthor(): Author {
    return Author(
        name = name,
        username = username,
        avatarPath = avatarPath,
        rating = rating,
    )
}