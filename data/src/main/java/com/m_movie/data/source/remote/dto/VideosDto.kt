package com.m_movie.data.source.remote.dto

import com.google.gson.annotations.SerializedName
import com.m_movie.domain.model.Videos

/***
 * Created By Mohammad Toriq on 04/02/2024
 */
data class VideosDto (
    @SerializedName("results")
    var videos: List<VideoDto> = listOf(),
)

fun VideosDto.toVideos(): Videos {
    return Videos(
        videos = videos.map { it.toVideo() }.toList(),
    )
}