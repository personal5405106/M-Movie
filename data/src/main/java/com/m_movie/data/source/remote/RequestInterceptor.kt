package com.m_movie.data.source.remote

import okhttp3.Interceptor
import okhttp3.Response

/***
 * Created By Mohammad Toriq on 04/02/2024
 */
class RequestInterceptor : Interceptor {
    override fun intercept(chain: Interceptor.Chain): Response {
        val originalRequest = chain.request()
        val newUrl = originalRequest.url
            .newBuilder()
            .build()
        val request = originalRequest.newBuilder()
            .url(newUrl)
            .addHeader("Authorization","bearer "+ApiService.TOKEN)
            .build()
        return chain.proceed(request)
    }
}