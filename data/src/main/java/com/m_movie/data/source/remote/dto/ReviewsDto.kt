package com.m_movie.data.source.remote.dto

import com.google.gson.annotations.SerializedName
import com.m_movie.domain.model.Review
import com.m_movie.domain.model.Reviews

/***
 * Created By Mohammad Toriq on 04/02/2024
 */
data class ReviewsDto (
    @SerializedName("page")
    var page: Long = 0,
    @SerializedName("results")
    var reviews: List<ReviewDto> = listOf(),
    @SerializedName("total_pages")
    var totalPages: Long = 0,
    @SerializedName("total_results")
    var totalResults: Long = 0,
)

fun ReviewsDto.toReviews(): Reviews {
    var arrayList = ArrayList<Review>()
    var list = reviews.map { it.toReview() }
    list.forEach {
        arrayList.add(it)
    }
    return Reviews(
        page = page,
        reviews = arrayList,
        totalPages = totalPages,
        totalResults = totalResults,
    )
}