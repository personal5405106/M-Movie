package com.m_movie.data.repository

import android.util.Log
import com.m_movie.data.source.remote.ApiService
import com.m_movie.data.source.remote.dto.toGenres
import com.m_movie.data.source.remote.dto.toMovie
import com.m_movie.data.source.remote.dto.toMovies
import com.m_movie.data.source.remote.dto.toReviews
import com.m_movie.data.source.remote.dto.toVideos
import com.m_movie.domain.model.Genres
import com.m_movie.domain.model.Movie
import com.m_movie.domain.model.Movies
import com.m_movie.domain.model.Reviews
import com.m_movie.domain.model.Videos
import com.m_movie.domain.repository.Repository
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import javax.inject.Inject
import javax.inject.Singleton

/***
 * Created By Mohammad Toriq on 04/02/2024
 */
@Singleton
class RepositoryImpl @Inject constructor(
    private val apiService: ApiService
) : Repository  {
    override suspend fun getDiscoverMovies(page: Int, withGenres: String?): Flow<Movies> {
        return flow {
            var data = apiService.getDiscoverMovies(page,withGenres)
            emit(data.toMovies())
        }
    }

    override suspend fun getGenres(): Flow<Genres> {
        return flow {
            emit(apiService.getGenres().toGenres())
        }
    }

    override suspend fun getDetailMovie(id: Long): Flow<Movie> {
        return flow {
            emit(apiService.getDetailMovie(id).toMovie())
        }
    }

    override suspend fun getMovieReviews(id: Long, page: Int): Flow<Reviews> {
        return flow {
            var data = apiService.getMovieReviews(id,page)
            emit(data.toReviews())
        }
    }

    override suspend fun getMovieVideos(id: Long): Flow<Videos> {
        return flow {
            var data = apiService.getMovieVideos(id)
            emit(data.toVideos())
        }
    }

}