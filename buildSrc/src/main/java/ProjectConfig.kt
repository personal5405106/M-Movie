/***
 * Created By Mohammad Toriq on 03/02/2024
 */
object ProjectConfig {
    const val appId = "com.m_movie.app"
    const val minSdk = 24
    const val targetSdk = 33
    const val compileSdk = 34
    const val versionCode = 1
    const val versionName = "1.0"
}