/***
 * Created By Mohammad Toriq on 03/02/2024
 */
object Versions{
    const val core = "1.9.0"
    const val coreSplashscreen = "1.0.1"
    const val appcompat = "1.6.1"
    const val androidMaterial = "1.11.0"

    const val coroutinesLifecycleScope = "2.6.2"

    const val dagger = "2.45"
    const val hiltComposeNavigation = "1.0.0"

    const val roomVersion = "2.6.0"

    const val composeActivity = "1.8.2"
    const val composeBom = "2023.03.00"
    const val composeNavigation = "2.5.3"

    //TestImplementation
    const val junit = "4.13.2"

    const val testJunit = "1.1.5"
    const val testEspresso = "3.5.1"

    const val retrofit = "2.9.0"
    const val gsonConverter = "2.9.0"
    const val okhttp = "4.11.0"

    const val coil = "2.5.0"

    const val swipeRefresh = "0.23.1"
    const val systemUiController = "0.33.2-alpha"

    const val androidyoutubeplayer = "12.1.0"
}