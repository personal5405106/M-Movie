package com.m_movie.domain.model

/***
 * Created By Mohammad Toriq on 04/02/2024
 */
data class Movies (
    var page: Long = 0,
    var movies: ArrayList<Movie> = arrayListOf(),
    var totalPages: Long = 0,
    var totalResults: Long = 0,
)