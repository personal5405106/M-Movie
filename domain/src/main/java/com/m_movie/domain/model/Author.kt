package com.m_movie.domain.model

/***
 * Created By Mohammad Toriq on 04/02/2024
 */
data class Author(
    var name: String? = null,
    var username: String? = null,
    var avatarPath: String? = null,
    var rating: Double? = null,
)