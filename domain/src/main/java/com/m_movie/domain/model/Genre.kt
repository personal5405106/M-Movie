package com.m_movie.domain.model

/***
 * Created By Mohammad Toriq on 04/02/2024
 */
data class Genre(
    var id: Long = 0,
    var name: String? = null,
)