package com.m_movie.domain.model

/***
 * Created By Mohammad Toriq on 04/02/2024
 */
data class Video (
    var iso_639_1: String? = null,
    var iso_3166_1: String? = null,
    var name: String? = null,
    var key: String? = null,
    var site: String? = null,
    var size: Int = 0,
    var type: String? = null,
    var official: Boolean? = null,
    var publishedAt: String? = null,
    var id: String? = null,
)