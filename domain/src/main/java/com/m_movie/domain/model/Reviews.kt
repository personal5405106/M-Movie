package com.m_movie.domain.model

/***
 * Created By Mohammad Toriq on 04/02/2024
 */
data class Reviews (
    var page: Long = 0,
    var reviews: ArrayList<Review> = arrayListOf(),
    var totalPages: Long = 0,
    var totalResults: Long = 0,
)