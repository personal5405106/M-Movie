package com.m_movie.domain.model

/***
 * Created By Mohammad Toriq on 04/02/2024
 */
data class Review (
    var id: String? = null,
    var author: String? = null,
    var authorDetails: Author,
    var content: String? = null,
    var createdAt: String? = null,
    var updatedAt: String? = null,
    var url: String? = null,
)