package com.m_movie.domain.model

/***
 * Created By Mohammad Toriq on 04/02/2024
 */
data class Movie (
    var id: Long = 0,
    var title: String? = null,
    var desc: String? = null,
    var originalTitle: String? = null,
    var popularity: Double = 0.0,
    var imageUrlBackDrop: String? = null,
    var imageUrl: String? = null,
    var voteAverage: Float = 0f,
    var voteCount: Long = 0,
    var releaseDate: String? = null,
    var runtime: Int = 0,
    var budget: Long = 0,
    var genres: List<Genre> = listOf(),
    var homepage: String? = null,
    var status: String? = null,
)