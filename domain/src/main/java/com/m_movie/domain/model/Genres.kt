package com.m_movie.domain.model

/***
 * Created By Mohammad Toriq on 04/02/2024
 */
data class Genres(
    var genres : List<Genre> = listOf(),
)