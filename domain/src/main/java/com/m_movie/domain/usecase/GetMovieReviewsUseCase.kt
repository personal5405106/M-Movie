package com.m_movie.domain.usecase

import com.m_movie.domain.model.Movies
import com.m_movie.domain.model.Reviews
import com.m_movie.domain.repository.Repository
import kotlinx.coroutines.flow.Flow
import javax.inject.Inject

/***
 * Created By Mohammad Toriq on 04/02/2024
 */
class GetMovieReviewsUseCase @Inject constructor(
    private val repository: Repository
):BaseUseCase<List<String> , Flow<Reviews>>(){
    override suspend fun execute(params: List<String>): Flow<Reviews> {
        return repository.getMovieReviews(params[0].toLong(), params[1].toInt())
    }
}