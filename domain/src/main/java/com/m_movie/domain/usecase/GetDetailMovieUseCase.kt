package com.m_movie.domain.usecase

import com.m_movie.domain.model.Movie
import com.m_movie.domain.model.Videos
import com.m_movie.domain.repository.Repository
import kotlinx.coroutines.flow.Flow
import javax.inject.Inject

/***
 * Created By Mohammad Toriq on 04/02/2024
 */
class GetDetailMovieUseCase @Inject constructor(
    private val repository: Repository
):BaseUseCase<Long , Flow<Movie>>(){
    override suspend fun execute(params: Long): Flow<Movie> {
        return repository.getDetailMovie(params)
    }
}