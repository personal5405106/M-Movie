package com.m_movie.domain.usecase

import com.m_movie.domain.model.Genres
import com.m_movie.domain.repository.Repository
import kotlinx.coroutines.flow.Flow
import javax.inject.Inject

/***
 * Created By Mohammad Toriq on 04/02/2024
 */
class GetGenresUseCase @Inject constructor(
    private val repository: Repository
) :BaseUseCase<Unit , Flow<Genres>>(){
    override suspend fun execute(params: Unit): Flow<Genres> {
        return repository.getGenres()
    }
}