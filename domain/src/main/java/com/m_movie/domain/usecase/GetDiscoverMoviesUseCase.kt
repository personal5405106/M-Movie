package com.m_movie.domain.usecase

import android.util.Log
import com.m_movie.domain.model.Genres
import com.m_movie.domain.model.Movies
import com.m_movie.domain.repository.Repository
import kotlinx.coroutines.flow.Flow
import javax.inject.Inject

/***
 * Created By Mohammad Toriq on 04/02/2024
 */
class GetDiscoverMoviesUseCase @Inject constructor(
    private val repository: Repository
):BaseUseCase<List<String> , Flow<Movies>>(){
    override suspend fun execute(params: List<String>): Flow<Movies> {
        return repository.getDiscoverMovies(params[0].toInt(), params[1])
    }
}