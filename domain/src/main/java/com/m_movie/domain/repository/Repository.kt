package com.m_movie.domain.repository

import com.m_movie.domain.model.Genres
import com.m_movie.domain.model.Movie
import com.m_movie.domain.model.Movies
import com.m_movie.domain.model.Reviews
import com.m_movie.domain.model.Videos
import kotlinx.coroutines.flow.Flow

/***
 * Created By Mohammad Toriq on 04/02/2024
 */
interface Repository {
    suspend fun getDiscoverMovies(
        page: Int,
        withGenres: String?
    ): Flow<Movies>

    suspend fun getGenres(): Flow<Genres>

    suspend fun getDetailMovie(
        id: Long,
    ): Flow<Movie>

    suspend fun getMovieReviews(
        id: Long,
        page: Int,
    ): Flow<Reviews>

    suspend fun getMovieVideos(
        id: Long,
    ): Flow<Videos>
}