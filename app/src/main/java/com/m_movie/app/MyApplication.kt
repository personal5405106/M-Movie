package com.m_movie.app

import android.app.Application
import dagger.hilt.android.HiltAndroidApp

/***
 * Created By Mohammad Toriq on 04/02/2024
 */

@HiltAndroidApp
class MyApplication : Application()